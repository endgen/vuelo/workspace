import { createConfigForNuxt } from '@nuxt/eslint-config/flat'

const ignores = [
  '**/graphql/client/*.ts',
  '**/sandbox/**',
  '**/stories/**',
]

const globals = {
  ignores
}

export default createConfigForNuxt({
  features: {
    tooling: true
  }
}).append({
  ...globals,
  rules: {
    "no-debugger": "warn",
    "no-console": ["error", {
      allow: ["info", "warn", "error"],
    }],
  }
}).override('nuxt/typescript/rules', {
  ...globals,
  rules: {
    // https://typescript-eslint.io/rules
    "@typescript-eslint/quotes": ["error", "single"],
    "@typescript-eslint/semi": ["error", "never"],
    "@typescript-eslint/ban-ts-comment": "off",
    "@typescript-eslint/no-explicit-any": "off",

    "@typescript-eslint/no-unused-vars": ["warn", {
        varsIgnorePattern: "^_.*",
        argsIgnorePattern: "^_.*",
    }],
  }
}).override('nuxt/tooling/unicorn', {
  ...globals,
  rules: {
    // https://github.com/sindresorhus/eslint-plugin-unicorn/blob/main/docs/rules
    "unicorn/prefer-module": "off",
    "unicorn/prevent-abbreviations": "off",
    "unicorn/no-console-spaces": "off",
    "unicorn/explicit-length-check": "off",
    "unicorn/numeric-separators-style": "warn",
    "unicorn/no-null": "off",
    "unicorn/no-array-reduce": "off",

    "unicorn/filename-case": ["error", {
      cases: {
        kebabCase: true,
        pascalCase: true,
      },
    }],
  },
}).override('nuxt/vue/rules', {
  ...globals,
  rules: {
    "vue/v-slot-style": "off",
    "vue/valid-v-slot": "off",
    "vue/valid-template-root": "off",
    "vue/html-self-closing": "off",
    "vue/no-v-html": "off",
    "vue/no-multiple-template-root": "warn",
    "vue/multi-word-component-names": "off",
    "vue/multi-word-component-names": "off",
  }
});
