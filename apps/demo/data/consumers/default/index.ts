// @ts-ignore
import type { EachMessagePayload } from 'kafkajs'
import type { DemoMessage } from '#gql/client/graphql'


export default {

  'demo-events': {

    'demo.messages': {
      subscriber:
        {
          async eachMessage({ message, topic: _topic, partition: _partition }: EachMessagePayload) {
            const demoMessages: DemoMessage = await useConsumerDataClient<DemoMessage>().withSchema('demo').select('id').from('messages')
            console.info('events', JSON.parse(String(message.value))?.payload)
            console.info('DemoMessages', demoMessages)
          }
        }
    },

  },

  'demo-integration': {

    'demo.messages': {
      subscriber:
        {
          async eachMessage({ message, topic: _topic, partition: _partition }: EachMessagePayload) {
            console.info('integration', JSON.parse(String(message.value))?.payload)
          }
        }
    },

  },

}
