'''
Test - schema (demo)
'''
# Revision ID: 79ef25dab136
# Previous ID:
# Created: 2024-07-07 16:37:07.065711
#
# Tips:
#
#   1)  Make schema changes in pgAdmin first, then use pgAdmin's Schema Diff tool between the main database and the shadow database.
#       Alternatively, you can use use option "-d" (or "--diff") during "sdk migrate create" command to auto-generate SQL
#       that can be copy/pasted from the command line into this migration file as well.
#
#       DISCLAIMER: *Always check auto-generated statements*, and never _blindly_ use the auto-generated code for your migrations!
#
#   2)  Use `op.execute(sqltext: Executable | str)` to run raw SQL statements.
#       See: https://alembic.sqlalchemy.org/en/latest/ops.html#alembic.operations.Operations.execute
#
#   3)  Install the "better-python-string-sql" extension for VSCode for SQL syntax highlighting within Python strings!
#       See: https://marketplace.visualstudio.com/items?itemName=Submersible.better-python-string-sql
#
#   4)  You may use various `alembic.operations.Operations` methods instead of using raw SQL for most operations.
#       See: https://alembic.sqlalchemy.org/en/latest/ops.html#alembic.operations.Operations
#
#   5)  In this file, `Schema` class is used to update database schema; `Seed` class is used to add initial data after `Schema` operations have run;
#       and `Mock` class adds mock test data after `Seed` operations have run (this order is reversed when downgrading.)
#
#       Implement each class' `up(db)` and `down(db)` methods; implementation for all methods is optional (it's possible to have an empty migration file.)
#
#       IMPORTANT: *Mock hydration should only be used in development and test environments*, and is configurable with the `DB_HYDRATION`
#       environment variable, which supports the following command-separated enum string: 'seed' | 'mock' | 'development' | 'all'; default value is: None
#
#   6)  Data files (CSV, JSON, etc.) located in your "migrations" root directory, relative to this file, can be read in using `kwargs['current_directory']`.
#       This may be useful for hydrating seed or mock data.

import os
import re
import urllib.parse
import sqlalchemy as sa
from alembic import op, context



# Data schema migration
class Schema:

  @staticmethod
  def up(db, **kwargs):
    op.execute('''
      CREATE TABLE IF NOT EXISTS demo.forums (
        "id" varchar(36),
        "title" varchar(100) NOT NULL,
        primary key ("id")
      );

      GRANT ALL ON TABLE demo.forums TO graphql;

      CREATE TABLE IF NOT EXISTS demo.messages (
        "id" varchar(36),
        "body" varchar(100) NOT NULL,
        "forum" varchar(36) references demo.forums ("id"),
        primary key ("id")
      );

      GRANT ALL ON TABLE demo.messages TO graphql;

      CREATE TRIGGER graphql_insert
      AFTER INSERT ON demo.messages
      FOR EACH ROW
      EXECUTE PROCEDURE common.graphql_subscription(
        'create', -- the "event" string, useful for the client to know what happened
        'demo:forum:$1:message', -- the "topic" the event will be published to, as a template
        'forum' -- If specified, `$1` above will be replaced with NEW.id or OLD.id from the trigger.
      );

      CREATE TRIGGER graphql_update
      AFTER UPDATE ON demo.messages
      FOR EACH ROW
      EXECUTE PROCEDURE common.graphql_subscription(
        'update', -- the "event" string, useful for the client to know what happened
        'demo:forum:$1:message', -- the "topic" the event will be published to, as a template
        'forum' -- If specified, `$1` above will be replaced with NEW.id or OLD.id from the trigger.
      );

      CREATE TRIGGER graphql_delete
      AFTER DELETE ON demo.messages
      FOR EACH ROW
      EXECUTE PROCEDURE common.graphql_subscription(
        'delete', -- the "event" string, useful for the client to know what happened
        'demo:forum:$1:message', -- the "topic" the event will be published to, as a template
        'forum' -- If specified, `$1` above will be replaced with NEW.id or OLD.id from the trigger.
      );
    ''')


  @staticmethod
  def down(db, **kwargs):
    op.execute('''
      DROP TABLE IF EXISTS demo.messages CASCADE;
      DROP TABLE IF EXISTS demo.forums CASCADE;
    ''')



# Seed data hydration
class Seed:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Mock data hydration
class Mock:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Development data hydration
class Development:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Used by migration framework
revision = '79ef25dab136'
down_revision = None
depends_on = None
branch_labels = ('demo',)

def get_hydration_config():
  hydration = os.getenv("DB_HYDRATION")
  return re.split(r",\s*", hydration) if hydration else []

def upgrade(engine_name):
  dir = os.path.dirname(os.path.abspath(__file__))
  db = urllib.parse.urlparse(engine_name)
  hydration = get_hydration_config()

  Schema.up(db, current_directory=dir)

  if "all" in hydration or "development" in hydration:
    Development.up(db, current_directory=dir)

  if "all" in hydration or "seed" in hydration:
    Seed.up(db, current_directory=dir)

  if "all" in hydration or "mock" in hydration:
    Mock.up(db, current_directory=dir)

def downgrade(engine_name):
  dir = os.path.dirname(os.path.abspath(__file__))
  db = urllib.parse.urlparse(engine_name)
  hydration = get_hydration_config()

  if "all" in hydration or "mock" in hydration:
    Mock.down(db, current_directory=dir)

  if "all" in hydration or "seed" in hydration:
    Seed.down(db, current_directory=dir)

  if "all" in hydration or "development" in hydration:
    Development.down(db, current_directory=dir)

  Schema.down(db, current_directory=dir)
