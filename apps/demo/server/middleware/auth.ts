export default defineEventHandler((event) => {
  // TODO: Verify and decode OAuth token

  // If making a query, pass along JWT role and claims to database connection
  if(event.path === '/graphql') {
    event.context.vuelo = {
      ...event.context.vuelo,
      db: {
        // role: 'graphql',
        ['jwt.claims.a']: 1
      }
    }
  }
})