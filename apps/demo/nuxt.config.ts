// Define unique development server port per each app.
// Don't forget to expose this port in `compose.yml`.
const port = 3001

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  devServer: {
    port
  },

  vite: {
    server: {
      hmr: {
        // Addresses a very minor Nuxt bug on Nuxt config reload
        // sometimes falling back to 24678 WS port
        port
      }
    }
  },

  extends: [
    `${process.env.SDK_LAYER_PATH}/common`
  ],

  vuelo: {
    graphql: {
      schema: [
        'https://rickandmortyapi.com/graphql'
      ]
    }
  },

  $production: {
    vuelo: {
      graphql: {
        build: {
          output: true
        }
      }
    }
  }
})
