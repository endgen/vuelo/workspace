// @ts-ignore
import { makeExtendSchemaPlugin, gql } from 'postgraphile/utils'
// @ts-ignore
import { context, _lambda, listen } from 'postgraphile/grafast'
// @ts-ignore
import { jsonParse } from 'postgraphile/@dataplan/json'
// @ts-ignore
// import { EXPORTABLE } from 'graphile-export'

export default [
  makeExtendSchemaPlugin((build: any) => {
    const { demo_messages } = build.input.pgRegistry.pgResources

    return {
      typeDefs: /* GraphQL */ gql`
        extend type Subscription {
          forumMessage(forumId: Int!): ForumMessageSubscriptionPayload
        }

        type ForumMessageSubscriptionPayload {
          event: String
          message: DemoMessage
        }
      `,
      plans: {
        Subscription: {
          forumMessage: {
            // subscribePlan: EXPORTABLE(
            //   (lambda, context, listen, jsonParse) => (_$root, args) => {
            //     const $pgSubscriber = context().get("pgSubscriber")
            //     const $forumId = args.get("forum")
            //     const $topic = lambda($forumId, (id) => `demo:forum:${id}:message`)
            //     return listen($pgSubscriber, $topic, jsonParse)
            //   },
            //   [lambda, context, listen, jsonParse]
            // ),
            subscribePlan(_$root: any, _args: any) {
              const $pgSubscriber = context().get('pgSubscriber')
              // const $forumId = args.get("forumId")
              // const $topic = lambda($forumId, (id) => `demo:forum:${id}:message`)
              const $topic = 'demo:forum:1:message'
              return listen($pgSubscriber, $topic, jsonParse)
            },
            plan($event: any) {
              return $event
            },
          },
        },
        ForumMessageSubscriptionPayload: {
          event($event: any) {
            return $event.get('event')
          },
          message($event: any) {
            const $id = $event.get('id')
            return demo_messages.get({ id: $id })
          },
        },
      },
    }
  }),
]
