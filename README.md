# Endgen Vuelo™

Rapid enterprise-grade web app SDK, batteries included.

Built for the CodeSpark™ marketplace,
exclusively by the Endgen team 🚀

## Recommended Instance Types

| Type | Price<sup>*</sup> | vCPUs | RAM |
| ---      | ---      | ---  | ---  |
| t3.xlarge   | USD 0.1664   | 4  | 16 GB  |
| t3a.xlarge   | USD 0.1504   | 4  | 16 GB |
| c5.xlarge   | USD 0.17   | 4  | 8 GB  |
| c4.xlarge   | USD 0.199   | 4  | 8 GB  |

\* *Prices as of July 2024*

## Instance Activity Scheduling

We recommend defining a CodeSpark schedule to stop instances during non-work hours.

For example, only 10 hour uptime per day (recommended) would decrease instance hosting bills by ~60%.

## Environment Variables

| Variable | Options | Description |
| ---      | ---      | ---      |
| SDK_HMR_WS_PROTOCOL   | None \| "wss"    | Set to "wss" if using HTTPS   |

## SSH Config

```
# ~/.ssh/config

...

Host <YOUR_PROVIDED_CODESPARK_SSH_CONFIG_HOST_NAME>
    LogLevel QUIET
    ForwardAgent yes
    RequestTTY yes
    User agent
    IdentityFile ~/.ssh/<YOUR_CODESPARK_PRIVATE_KEY_FILE>
    HostName <YOUR_CODESPARK_ORGANIZATION_HOSTNAME>.endgen.net
    Port <YOUR_ASSIGNED_CODESPARK_PORT_NUMBER>
    LocalForward 7000 localhost:7000
    LocalForward 6006 localhost:6006
    LocalForward 6007 localhost:6007
    LocalForward 3000 localhost:3000
    LocalForward 9000 localhost:9000
    LocalForward 3001 localhost:3001
    # Add more 300X ports as more apps are added to the SDK
```

## Setup

If setting up for the first time, run (from your host terminal):

`$ ssh -q <YOUR_PROVIDED_CODESPARK_SSH_CONFIG_HOST_NAME> codespark init [WORKSPACE_STARTER_REPO_URL]`

`WORKSPACE_STARTER_REPO_URL` is optional, if not provided, CodeSpark will use a project-defined starter repository, if any.

## Connecting

From your host terminal:

`$ ssh -q <YOUR_PROVIDED_CODESPARK_SSH_CONFIG_HOST_NAME>`

## Running

From within the remote terminal:

`$ docker compose up -d`

OR (if using remote load balancer with HTTPS),

`$ SDK_HMR_WS_PROTOCOL=wss docker compose up -d`

You may create a `.env` file as well for environment variables.

To view logs:

`$ docker compose logs --tail 50 -f vuelo`

## Forwarding SSH Keys

You must use Git from within the CodeSpark instance using `git` CLI (Git GUI tools not supported.)

You can add SSH keys on the instance, or use an automatically provided key (~/.ssh/id_rsa) on the remote instance, or forward your own keys from your host.

To forward your own SSH keys, from your host terminal, run these commands before establishing SSH connection in the current terminal screen:

`$ eval $(ssh-agent -s)`

`$ ssh-add`

You must do this for *any* SSH session from a host terminal screen (new screens need to have `ssh-add` run prior to connecting on *that* terminal screen.)

Typically, though, you'll only ever need one screen for Git operations (not every remote terminal session needs SSH keys forwarded; only, really, applies to the screen you're going to be running Git commands on.)

## Mounting Remote File System

Mounting files requires SSHFS to be installed.

To install SSHFS, run (from your host terminal):

`$ sudo apt install sshfs`

Once SSHFS has been installed, run (from your host terminal) to connect:

`$ mkdir -p <YOUR_PREFERRED_HOST_MOUNT_DIRECTORY>`

`$ sshfs dev@<YOUR_PROVIDED_CODESPARK_SSH_CONFIG_HOST_NAME>:. <YOUR_PREFERRED_HOST_MOUNT_DIRECTORY> -o reconnect,cache=no`

**Important!** Do **not** `cd` into, or do any file operations in, the immediate directory; use the parent directory. For example:

```
PARENT_DIRECTORY=~/My-Organization
YOUR_PREFERRED_HOST_MOUNT_DIRECTORY=$PARENT_DIRECTORY/My-Project
code $PARENT_DIRECTORY  # `code` opens VSCode; use terminal and SSH commands from PARENT_DIRECTORY
```

## Unmounting Remote File System

`$ sudo umount <YOUR_PREFERRED_HOST_MOUNT_DIRECTORY> -l`

`$ rm -rf <YOUR_PREFERRED_HOST_MOUNT_DIRECTORY>`

## Updating Host Instance

`$ ssh -q <YOUR_PROVIDED_CODESPARK_SSH_CONFIG_HOST_NAME> codespark update`
