import type { EachMessagePayload } from 'kafkajs'
// import type { MyType } from '#gql/client/graphql'


export default {

  'common-events': {

    'my-schema.my-table': {
      subscriber:
        {
          async eachMessage({ message, topic: _topic, partition: _partition }: EachMessagePayload) {
            // const myData: MyType = await useConsumerDataClient<MyType>()
            //   .withSchema('my-schema')
            //   .select('id')
            //   .from('my-table')

            console.info(JSON.parse(String(message.value))?.payload)
          }
        }
    },

  }

}
