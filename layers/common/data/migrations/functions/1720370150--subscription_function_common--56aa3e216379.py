'''
Subscription - function (common)
'''
# Revision ID: 4fe56a3b00f1
# Previous ID:
# Created: 2024-07-07 16:25:36.734199
#
# Tips:
#
#   1)  Make schema changes in pgAdmin first, then use pgAdmin's Schema Diff tool between the main database and the shadow database.
#       Alternatively, you can use use option "-d" (or "--diff") during "sdk migrate create" command to auto-generate SQL
#       that can be copy/pasted from the command line into this migration file as well.
#
#       DISCLAIMER: *Always check auto-generated statements*, and never _blindly_ use the auto-generated code for your migrations!
#
#   2)  Use `op.execute(sqltext: Executable | str)` to run raw SQL statements.
#       See: https://alembic.sqlalchemy.org/en/latest/ops.html#alembic.operations.Operations.execute
#
#   3)  Install the "better-python-string-sql" extension for VSCode for SQL syntax highlighting within Python strings!
#       See: https://marketplace.visualstudio.com/items?itemName=Submersible.better-python-string-sql
#
#   4)  You may use various `alembic.operations.Operations` methods instead of using raw SQL for most operations.
#       See: https://alembic.sqlalchemy.org/en/latest/ops.html#alembic.operations.Operations
#
#   5)  In this file, `Schema` class is used to update database schema; `Seed` class is used to add initial data after `Schema` operations have run;
#       and `Mock` class adds mock test data after `Seed` operations have run (this order is reversed when downgrading.)
#
#       Implement each class' `up(db)` and `down(db)` methods; implementation for all methods is optional (it's possible to have an empty migration file.)
#
#       IMPORTANT: *Mock hydration should only be used in development and test environments*, and is configurable with the `DB_HYDRATION`
#       environment variable, which supports the following command-separated enum string: 'seed' | 'mock' | 'development' | 'all'; default value is: None
#
#   6)  Data files (CSV, JSON, etc.) located in your "migrations" root directory, relative to this file, can be read in using `kwargs['current_directory']`.
#       This may be useful for hydrating seed or mock data.

import os
import re
import urllib.parse
import sqlalchemy as sa
from alembic import op, context



# Data schema migration
class Schema:

  @staticmethod
  def up(db, **kwargs):
    op.execute('''
      CREATE FUNCTION common.graphql_subscription() RETURNS trigger
          LANGUAGE plpgsql
          AS $_$
      declare
        v_process_new bool = (TG_OP = 'INSERT' OR TG_OP = 'UPDATE');
        v_process_old bool = (TG_OP = 'UPDATE' OR TG_OP = 'DELETE');
        v_event text = TG_ARGV[0];
        v_topic_template text = TG_ARGV[1];
        v_attribute text = TG_ARGV[2];
        v_record record;
        v_sub text;
        v_topic text;
        v_i int = 0;
        v_last_topic text;
      begin
        RAISE NOTICE 'Triggered';
        for v_i in 0..1 loop
          if (v_i = 0) and v_process_new is true then
            v_record = new;
          elsif (v_i = 1) and v_process_old is true then
            v_record = old;
          else
            continue;
          end if;
          if v_attribute is not null then
            execute 'select $1.' || quote_ident(v_attribute)
              using v_record
              into v_sub;
          end if;
          if v_sub is not null then
            v_topic = replace(v_topic_template, '$1', v_sub);
          else
            v_topic = v_topic_template;
          end if;
          if v_topic is distinct from v_last_topic then
            -- This if statement prevents us from triggering the same notification twice
            v_last_topic = v_topic;
            perform pg_notify(v_topic, json_build_object(
              'event', v_event,
              'subject', v_sub,
              'id', v_record.id
            )::text);
          end if;
        end loop;
        return v_record;
      end;
      $_$;
    ''')

  @staticmethod
  def down(db, **kwargs):
    op.execute('''
      DROP FUNCTION IF EXISTS common.graphql_subscription CASCADE;
    ''')



# Seed data hydration
class Seed:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Mock data hydration
class Mock:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Development data hydration
class Development:

  @staticmethod
  def up(db, **kwargs):
    pass

  @staticmethod
  def down(db, **kwargs):
    pass



# Used by migration framework
revision = '4fe56a3b00f1'
down_revision = None
depends_on = None
branch_labels = ('common',)

def get_hydration_config():
  hydration = os.getenv("DB_HYDRATION")
  return re.split(r",\s*", hydration) if hydration else []

def upgrade(engine_name):
  dir = os.path.dirname(os.path.abspath(__file__))
  db = urllib.parse.urlparse(engine_name)
  hydration = get_hydration_config()

  Schema.up(db, current_directory=dir)

  if "all" in hydration or "development" in hydration:
    Development.up(db, current_directory=dir)

  if "all" in hydration or "seed" in hydration:
    Seed.up(db, current_directory=dir)

  if "all" in hydration or "mock" in hydration:
    Mock.up(db, current_directory=dir)

def downgrade(engine_name):
  dir = os.path.dirname(os.path.abspath(__file__))
  db = urllib.parse.urlparse(engine_name)
  hydration = get_hydration_config()

  if "all" in hydration or "mock" in hydration:
    Mock.down(db, current_directory=dir)

  if "all" in hydration or "seed" in hydration:
    Seed.down(db, current_directory=dir)

  if "all" in hydration or "development" in hydration:
    Development.down(db, current_directory=dir)

  Schema.down(db, current_directory=dir)
