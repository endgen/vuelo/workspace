// import type { Exchange } from '@urql/vue'
// import type { Operation } from '@urql/core'
import type { IntrospectionData } from '@urql/exchange-graphcache/dist/types/ast'

import { defineNuxtPlugin } from '#app'
import defu from 'defu'
import urql, { _dedupExchange, cacheExchange, fetchExchange, ssrExchange, subscriptionExchange } from '@urql/vue'
import { _makeOperation } from '@urql/core'
import { offlineExchange } from '@urql/exchange-graphcache'
import { requestPolicyExchange } from '@urql/exchange-request-policy'
import { makeDefaultStorage } from '@urql/exchange-graphcache/default-storage'
import { pipe, map, mergeMap, fromPromise, fromValue, _tap } from 'wonka'
import { createClient as createWSClient } from 'graphql-ws'

// @ts-ignore
import cache from '#build/vuelo/graphql/urql/cache'
// @ts-ignore
import schema from '#gql/client/schema.json'

// import useAuth, { init as auth } from '../composables/auth'

// const RE_SCHEME = /(?:http)(s?:\/\/)/
// const RE_ABS_PATH = /^\/(.*)/
// const RE_DOMAIN = /([^:]*).*/
// const RE_PORT = /[^:]*(?::(.*))?$/

const isPromise = (value: any) => value && typeof value.then === 'function'

// https://github.com/FormidableLabs/urql/issues/234#issuecomment-602305153
const fetchOptionsExchange = (fn: any): Exchange => ({
  forward
}) => ops$ => {
  return pipe(
    // @ts-ignore
    ops$,
    mergeMap((operation: Operation) => {
      const result = fn(operation.context.fetchOptions)
      return pipe(
        // @ts-ignore
        isPromise(result) ? fromPromise(result) : fromValue(result),
        map((fetchOptions: RequestInit | (() => RequestInit)) => ({
          ...operation,
          context: { ...operation.context, fetchOptions }
        }))
      )
    }),
    forward
  )
}


// @ts-ignore
export default defineNuxtPlugin(async ({ vueApp, ssrContext, hook }) => {
  const clientExchanges = []
  // const config = useRuntimeConfig().gql || {}
  // const { host } = useRuntimeConfig().api || {}
  // const url = `${host || (ssrContext ? `http://localhost:3000` : '')}/gql`
  const url = '/graphql'

  const ssr = ssrExchange({
    isClient: !ssrContext,
    // @ts-ignore
    initialState: ssrContext ? undefined : window.__NUXT__.data
  })

  // if(!ssrContext) {
  //   ssr.restoreData(JSON.parse(window.__NUXT__.data))
  // }

  hook('app:rendered', () => {
    if(ssrContext) {
      // @ts-ignore
      ssrContext.payload.data = JSON.stringify(ssr.extractData())
    }
  })

  // const routerExchange = ({ forward }) => {
  //   const route = (operation: Operation): Operation => {
  //     const url = (config?.exchanges?.routerExchange?.operations || []).reduce((url, config) => new RegExp(config.name).test(operation.query.definitions[0].name.value) ? config.url : url, null)

  //     if(url) {
  //       return makeOperation(operation.kind, operation, {
  //         ...operation.context,
  //         url
  //       })
  //     }

  //     return operation
  //   }

  //   return operations$ => {
  //     return pipe(
  //       operations$,
  //       map(route),
  //       forward
  //     )
  //   }
  // }

  // const getWSUrl = () => {
  //   if(window.location.port) {
  //     return `${url.replace(RE_ABS_PATH, `${window.location.protocol}//${window.location.host}/$1`).replace(RE_SCHEME, 'ws$1')}`.replace(`:${window.location.port}`, `:${config.subscriptionPort || 4000}`)
  //   } else {
  //     return `${url.replace(RE_ABS_PATH, `${window.location.protocol}//${window.location.host}/$1`).replace(RE_SCHEME, 'ws$1')}/ws`
  //   }
  // }

  if(!ssrContext) {4400
    const wsClient = createWSClient({
      url,
      // connectionParams: async () => {
      //   const auth = useAuth()

      //   let token

      //   try {
      //     if(auth.authenticated.value || (!ssrContext && auth.client && await auth.client.isAuthenticated())) {
      //       token = ssrContext ? (await import('h3')).useCookie(ssrContext.req, 'token') : auth.token.value
      //       token = token && ssrContext ? JSON.parse(token) : token
      //     }

      //     return token ? { Authorization: `Bearer ${token}` } : {}
      //   } catch(e) {
      //     console.error(e)
      //     return {}
      //   }
      //   return {}
      // }
    })

    clientExchanges.push(subscriptionExchange({
      forwardSubscription: (operation: any) => {
        return {
          subscribe: (sink: any) => {
            const unsubscribe = wsClient.subscribe(
              operation,
              {
                ...sink,
                next: (...args: any) => {
                  // console.log(...args)
                  return sink.next(...args)
                }
              }
            )

            return { unsubscribe }
          }
        }
      }
    }))
  }

  vueApp.use(urql, {
    url,
    requestPolicy: 'cache-and-network',
    suspense: !!ssrContext,
    exchanges: [
      // dedupExchange,
      requestPolicyExchange({
        // The amount of time in ms that has to go by before upgrading, default is 5 minutes.
        ttl: 60 * 1000 * 5, // 5 minutes.
        // An optional function that allows you to specify whether an operation should be upgraded.
        shouldUpgrade: (operation: any) => operation.context.requestPolicy !== 'cache-only',
      }),
      ssrContext || !schema.__schema ? cacheExchange : offlineExchange({
        schema: (schema as unknown) as IntrospectionData,
        storage: makeDefaultStorage({
          idbName: 'cache',
          maxAge: 14
        }),
        ...defu.apply(defu, cache.flat())
      }),
      ssr,
      // routerExchange,
      ...clientExchanges,
      // ({ forward }) => ops$ => {
      //   return pipe(
      //     forward(ops$),
      //     tap(async ({ data }) => {
      //       if(!ssrContext && data && data.errors && [403, 401].includes(data.errors[0].status)) {
      //         useAuth().logout({
      //           returnTo: window.location.origin
      //         })
      //         // await useAuth().refresh()

      //         // if(!useAuth().client.isAuthenticated()) {
      //         //   useAuth().logout({
      //         //     returnTo: window.location.origin
      //         //   })
      //         // }
      //       }
      //     })
      //   );
      // },
      fetchOptionsExchange(async (fetchOptions: any) => {
        // const auth = useAuth()

        // let token

        // try {
        //   if(auth.authenticated.value || (!ssrContext && auth.client && await auth.client.isAuthenticated())) {
        //     // if(!ssrContext) {
        //     //   const { iat, exp } = auth.user.value

        //     //   if(iat && exp && (1 - parseInt(exp - Date.now() / 1000) / (exp - iat) > .65)) {
        //     //     await auth.refresh()
        //     //   }
        //     // }
        //     token = ssrContext ? (await import('h3')).useCookie(ssrContext.req, 'token') : auth.token.value
        //     token = token && ssrContext ? JSON.parse(token) : token
        //   }

        //   return token ? { ...fetchOptions, headers: { Authorization: `Bearer ${token}` } } : fetchOptions
        // } catch(e) {
        //   console.error(e)
        //   return fetchOptions
        // }
        return fetchOptions
      }),
      fetchExchange
    ],
    fetchOptions: () => ({})
  })
})
