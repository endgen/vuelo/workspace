// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  // Expand list of default SDK layers
  extends: process.env.SDK_LAYERS.split(',')
})
