
declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DB_SUPERUSER_URL: string;
      DB_ADMIN_URL: string;
      DB_API_URL: string;
      DB_GRAPHQL_URL: string;
      DB_CONSUMER_URL: string;
      DB_CONNECTOR_URL: string;
      DB_HYDRATION: string;
      EVENT_BROKER_URLS: string;
      EVENT_CONNECTOR_URL: string;
      EVENT_CONSUMERS_ENABLED: string;
      EVENT_SESSION_TIMEOUT: string;
      GQL_SETTINGS_ENABLE: string;
      SDK_LAYERS: string;
      SDK_WORKSPACE_PATH: string;
      SDK_SANDBOX_PATH: string;
      SDK_APP_PATH: string;
      SDK_LAYER_PATH: string;
      SDK_MODULE_PATH: string;
    }
  }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {}
