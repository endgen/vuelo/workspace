import { GraphQLResolveInfo, GraphQLScalarType, GraphQLScalarTypeConfig } from 'graphql';
import gql from 'graphql-tag';
import * as VueApolloComposable from '@vue/apollo-composable';
import * as VueCompositionApi from '@vue/composition-api';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
export type Omit<T, K extends keyof T> = Pick<T, Exclude<keyof T, K>>;
export type RequireFields<T, K extends keyof T> = Omit<T, K> & { [P in K]-?: NonNullable<T[P]> };
export type ReactiveFunction<TParam> = () => TParam;
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  /** A location in a connection that can be used for resuming pagination. */
  Cursor: { input: any; output: any; }
  /** The `Upload` scalar type represents a file upload. */
  Upload: { input: any; output: any; }
};

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

export type Character = {
  __typename?: 'Character';
  /** Time at which the character was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** Episodes in which this character appeared. */
  episode: Array<Maybe<Episode>>;
  /** The gender of the character ('Female', 'Male', 'Genderless' or 'unknown'). */
  gender?: Maybe<Scalars['String']['output']>;
  /** The id of the character. */
  id?: Maybe<Scalars['ID']['output']>;
  /**
   * Link to the character's image.
   * All images are 300x300px and most are medium shots or portraits since they are intended to be used as avatars.
   */
  image?: Maybe<Scalars['String']['output']>;
  /** The character's last known location */
  location?: Maybe<Location>;
  /** The name of the character. */
  name?: Maybe<Scalars['String']['output']>;
  /** The character's origin location */
  origin?: Maybe<Location>;
  /** The species of the character. */
  species?: Maybe<Scalars['String']['output']>;
  /** The status of the character ('Alive', 'Dead' or 'unknown'). */
  status?: Maybe<Scalars['String']['output']>;
  /** The type or subspecies of the character. */
  type?: Maybe<Scalars['String']['output']>;
};

export type Characters = {
  __typename?: 'Characters';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Character>>>;
};

/** All input for the create `DemoForum` mutation. */
export type CreateDemoForumInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `DemoForum` to be created by this mutation. */
  demoForum: DemoForumInput;
};

/** The output of our create `DemoForum` mutation. */
export type CreateDemoForumPayload = {
  __typename?: 'CreateDemoForumPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `DemoForum` that was created by this mutation. */
  demoForum?: Maybe<DemoForum>;
  /** An edge for our `DemoForum`. May be used by Relay 1. */
  demoForumEdge?: Maybe<DemoForumEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `DemoForum` mutation. */
export type CreateDemoForumPayloadDemoForumEdgeArgs = {
  orderBy?: Array<DemoForumOrderBy>;
};

/** All input for the create `DemoMessage` mutation. */
export type CreateDemoMessageInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The `DemoMessage` to be created by this mutation. */
  demoMessage: DemoMessageInput;
};

/** The output of our create `DemoMessage` mutation. */
export type CreateDemoMessagePayload = {
  __typename?: 'CreateDemoMessagePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `DemoMessage` that was created by this mutation. */
  demoMessage?: Maybe<DemoMessage>;
  /** An edge for our `DemoMessage`. May be used by Relay 1. */
  demoMessageEdge?: Maybe<DemoMessageEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our create `DemoMessage` mutation. */
export type CreateDemoMessagePayloadDemoMessageEdgeArgs = {
  orderBy?: Array<DemoMessageOrderBy>;
};

/** All input for the `deleteDemoForumByRowId` mutation. */
export type DeleteDemoForumByRowIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  rowId: Scalars['String']['input'];
};

/** All input for the `deleteDemoForum` mutation. */
export type DeleteDemoForumInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `DemoForum` to be deleted. */
  id: Scalars['ID']['input'];
};

/** The output of our delete `DemoForum` mutation. */
export type DeleteDemoForumPayload = {
  __typename?: 'DeleteDemoForumPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedDemoForumId?: Maybe<Scalars['ID']['output']>;
  /** The `DemoForum` that was deleted by this mutation. */
  demoForum?: Maybe<DemoForum>;
  /** An edge for our `DemoForum`. May be used by Relay 1. */
  demoForumEdge?: Maybe<DemoForumEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `DemoForum` mutation. */
export type DeleteDemoForumPayloadDemoForumEdgeArgs = {
  orderBy?: Array<DemoForumOrderBy>;
};

/** All input for the `deleteDemoMessageByRowId` mutation. */
export type DeleteDemoMessageByRowIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  rowId: Scalars['String']['input'];
};

/** All input for the `deleteDemoMessage` mutation. */
export type DeleteDemoMessageInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** The globally unique `ID` which will identify a single `DemoMessage` to be deleted. */
  id: Scalars['ID']['input'];
};

/** The output of our delete `DemoMessage` mutation. */
export type DeleteDemoMessagePayload = {
  __typename?: 'DeleteDemoMessagePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  deletedDemoMessageId?: Maybe<Scalars['ID']['output']>;
  /** The `DemoMessage` that was deleted by this mutation. */
  demoMessage?: Maybe<DemoMessage>;
  /** An edge for our `DemoMessage`. May be used by Relay 1. */
  demoMessageEdge?: Maybe<DemoMessageEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our delete `DemoMessage` mutation. */
export type DeleteDemoMessagePayloadDemoMessageEdgeArgs = {
  orderBy?: Array<DemoMessageOrderBy>;
};

export type DemoForum = Node & {
  __typename?: 'DemoForum';
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  id: Scalars['ID']['output'];
  rowId: Scalars['String']['output'];
  title: Scalars['String']['output'];
};

/**
 * A condition to be used against `DemoForum` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type DemoForumCondition = {
  /** Checks for equality with the object’s `rowId` field. */
  rowId?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `DemoForum` values. */
export type DemoForumConnection = {
  __typename?: 'DemoForumConnection';
  /** A list of edges which contains the `DemoForum` and cursor to aid in pagination. */
  edges: Array<Maybe<DemoForumEdge>>;
  /** A list of `DemoForum` objects. */
  nodes: Array<Maybe<DemoForum>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `DemoForum` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `DemoForum` edge in the connection. */
export type DemoForumEdge = {
  __typename?: 'DemoForumEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `DemoForum` at the end of the edge. */
  node?: Maybe<DemoForum>;
};

/** An input for mutations affecting `DemoForum` */
export type DemoForumInput = {
  rowId: Scalars['String']['input'];
  title: Scalars['String']['input'];
};

/** Methods to use when ordering `DemoForum`. */
export enum DemoForumOrderBy {
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** Represents an update to a `DemoForum`. Fields that are set will be updated. */
export type DemoForumPatch = {
  rowId?: InputMaybe<Scalars['String']['input']>;
  title?: InputMaybe<Scalars['String']['input']>;
};

export type DemoMessage = Node & {
  __typename?: 'DemoMessage';
  body: Scalars['String']['output'];
  /** Reads a single `DemoForum` that is related to this `DemoMessage`. */
  demoForumByForum?: Maybe<DemoForum>;
  forum?: Maybe<Scalars['String']['output']>;
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  id: Scalars['ID']['output'];
  rowId: Scalars['String']['output'];
};

/**
 * A condition to be used against `DemoMessage` object types. All fields are tested
 * for equality and combined with a logical ‘and.’
 */
export type DemoMessageCondition = {
  /** Checks for equality with the object’s `rowId` field. */
  rowId?: InputMaybe<Scalars['String']['input']>;
};

/** A connection to a list of `DemoMessage` values. */
export type DemoMessageConnection = {
  __typename?: 'DemoMessageConnection';
  /** A list of edges which contains the `DemoMessage` and cursor to aid in pagination. */
  edges: Array<Maybe<DemoMessageEdge>>;
  /** A list of `DemoMessage` objects. */
  nodes: Array<Maybe<DemoMessage>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** The count of *all* `DemoMessage` you could get from the connection. */
  totalCount: Scalars['Int']['output'];
};

/** A `DemoMessage` edge in the connection. */
export type DemoMessageEdge = {
  __typename?: 'DemoMessageEdge';
  /** A cursor for use in pagination. */
  cursor?: Maybe<Scalars['Cursor']['output']>;
  /** The `DemoMessage` at the end of the edge. */
  node?: Maybe<DemoMessage>;
};

/** An input for mutations affecting `DemoMessage` */
export type DemoMessageInput = {
  body: Scalars['String']['input'];
  forum?: InputMaybe<Scalars['String']['input']>;
  rowId: Scalars['String']['input'];
};

/** Methods to use when ordering `DemoMessage`. */
export enum DemoMessageOrderBy {
  IdAsc = 'ID_ASC',
  IdDesc = 'ID_DESC',
  Natural = 'NATURAL',
  PrimaryKeyAsc = 'PRIMARY_KEY_ASC',
  PrimaryKeyDesc = 'PRIMARY_KEY_DESC'
}

/** Represents an update to a `DemoMessage`. Fields that are set will be updated. */
export type DemoMessagePatch = {
  body?: InputMaybe<Scalars['String']['input']>;
  forum?: InputMaybe<Scalars['String']['input']>;
  rowId?: InputMaybe<Scalars['String']['input']>;
};

export type Episode = {
  __typename?: 'Episode';
  /** The air date of the episode. */
  air_date?: Maybe<Scalars['String']['output']>;
  /** List of characters who have been seen in the episode. */
  characters: Array<Maybe<Character>>;
  /** Time at which the episode was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** The code of the episode. */
  episode?: Maybe<Scalars['String']['output']>;
  /** The id of the episode. */
  id?: Maybe<Scalars['ID']['output']>;
  /** The name of the episode. */
  name?: Maybe<Scalars['String']['output']>;
};

export type Episodes = {
  __typename?: 'Episodes';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Episode>>>;
};

export type FilterCharacter = {
  gender?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  species?: InputMaybe<Scalars['String']['input']>;
  status?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type FilterEpisode = {
  episode?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
};

export type FilterLocation = {
  dimension?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<Scalars['String']['input']>;
};

export type ForumMessageSubscriptionPayload = {
  __typename?: 'ForumMessageSubscriptionPayload';
  event?: Maybe<Scalars['String']['output']>;
  message?: Maybe<DemoMessage>;
};

export type Info = {
  __typename?: 'Info';
  /** The length of the response. */
  count?: Maybe<Scalars['Int']['output']>;
  /** Number of the next page (if it exists) */
  next?: Maybe<Scalars['Int']['output']>;
  /** The amount of pages. */
  pages?: Maybe<Scalars['Int']['output']>;
  /** Number of the previous page (if it exists) */
  prev?: Maybe<Scalars['Int']['output']>;
};

export type Location = {
  __typename?: 'Location';
  /** Time at which the location was created in the database. */
  created?: Maybe<Scalars['String']['output']>;
  /** The dimension in which the location is located. */
  dimension?: Maybe<Scalars['String']['output']>;
  /** The id of the location. */
  id?: Maybe<Scalars['ID']['output']>;
  /** The name of the location. */
  name?: Maybe<Scalars['String']['output']>;
  /** List of characters who have been last seen in the location. */
  residents: Array<Maybe<Character>>;
  /** The type of the location. */
  type?: Maybe<Scalars['String']['output']>;
};

export type Locations = {
  __typename?: 'Locations';
  info?: Maybe<Info>;
  results?: Maybe<Array<Maybe<Location>>>;
};

/** The root mutation type which contains root level fields which mutate data. */
export type Mutation = {
  __typename?: 'Mutation';
  /** Creates a single `DemoForum`. */
  createDemoForum?: Maybe<CreateDemoForumPayload>;
  /** Creates a single `DemoMessage`. */
  createDemoMessage?: Maybe<CreateDemoMessagePayload>;
  /** Deletes a single `DemoForum` using its globally unique id. */
  deleteDemoForum?: Maybe<DeleteDemoForumPayload>;
  /** Deletes a single `DemoForum` using a unique key. */
  deleteDemoForumByRowId?: Maybe<DeleteDemoForumPayload>;
  /** Deletes a single `DemoMessage` using its globally unique id. */
  deleteDemoMessage?: Maybe<DeleteDemoMessagePayload>;
  /** Deletes a single `DemoMessage` using a unique key. */
  deleteDemoMessageByRowId?: Maybe<DeleteDemoMessagePayload>;
  /** Updates a single `DemoForum` using its globally unique id and a patch. */
  updateDemoForum?: Maybe<UpdateDemoForumPayload>;
  /** Updates a single `DemoForum` using a unique key and a patch. */
  updateDemoForumByRowId?: Maybe<UpdateDemoForumPayload>;
  /** Updates a single `DemoMessage` using its globally unique id and a patch. */
  updateDemoMessage?: Maybe<UpdateDemoMessagePayload>;
  /** Updates a single `DemoMessage` using a unique key and a patch. */
  updateDemoMessageByRowId?: Maybe<UpdateDemoMessagePayload>;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateDemoForumArgs = {
  input: CreateDemoForumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationCreateDemoMessageArgs = {
  input: CreateDemoMessageInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteDemoForumArgs = {
  input: DeleteDemoForumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteDemoForumByRowIdArgs = {
  input: DeleteDemoForumByRowIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteDemoMessageArgs = {
  input: DeleteDemoMessageInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationDeleteDemoMessageByRowIdArgs = {
  input: DeleteDemoMessageByRowIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateDemoForumArgs = {
  input: UpdateDemoForumInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateDemoForumByRowIdArgs = {
  input: UpdateDemoForumByRowIdInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateDemoMessageArgs = {
  input: UpdateDemoMessageInput;
};


/** The root mutation type which contains root level fields which mutate data. */
export type MutationUpdateDemoMessageByRowIdArgs = {
  input: UpdateDemoMessageByRowIdInput;
};

/** An object with a globally unique `ID`. */
export type Node = {
  /** A globally unique identifier. Can be used in various places throughout the system to identify this single value. */
  id: Scalars['ID']['output'];
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['Cursor']['output']>;
  /** When paginating forwards, are there more items? */
  hasNextPage: Scalars['Boolean']['output'];
  /** When paginating backwards, are there more items? */
  hasPreviousPage: Scalars['Boolean']['output'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['Cursor']['output']>;
};

/** The root query type which gives access points into the data universe. */
export type Query = Node & {
  __typename?: 'Query';
  /** Reads and enables pagination through a set of `DemoForum`. */
  allDemoForums?: Maybe<DemoForumConnection>;
  /** Reads and enables pagination through a set of `DemoMessage`. */
  allDemoMessages?: Maybe<DemoMessageConnection>;
  /** Get a specific character by ID */
  character?: Maybe<Character>;
  /** Get the list of all characters */
  characters?: Maybe<Characters>;
  /** Get a list of characters selected by ids */
  charactersByIds?: Maybe<Array<Maybe<Character>>>;
  /** Reads a single `DemoForum` using its globally unique `ID`. */
  demoForum?: Maybe<DemoForum>;
  /** Get a single `DemoForum`. */
  demoForumByRowId?: Maybe<DemoForum>;
  /** Reads a single `DemoMessage` using its globally unique `ID`. */
  demoMessage?: Maybe<DemoMessage>;
  /** Get a single `DemoMessage`. */
  demoMessageByRowId?: Maybe<DemoMessage>;
  /** Get a specific episode by ID */
  episode?: Maybe<Episode>;
  /** Get the list of all episodes */
  episodes?: Maybe<Episodes>;
  /** Get a list of episodes selected by ids */
  episodesByIds?: Maybe<Array<Maybe<Episode>>>;
  /** The root query type must be a `Node` to work well with Relay 1 mutations. This just resolves to `query`. */
  id: Scalars['ID']['output'];
  /** Get a specific locations by ID */
  location?: Maybe<Location>;
  /** Get the list of all locations */
  locations?: Maybe<Locations>;
  /** Get a list of locations selected by ids */
  locationsByIds?: Maybe<Array<Maybe<Location>>>;
  /** Fetches an object given its globally unique `ID`. */
  node?: Maybe<Node>;
  /**
   * Exposes the root query type nested one level down. This is helpful for Relay 1
   * which can only query top level fields if they are in a particular form.
   */
  query: Query;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllDemoForumsArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<DemoForumCondition>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<DemoForumOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryAllDemoMessagesArgs = {
  after?: InputMaybe<Scalars['Cursor']['input']>;
  before?: InputMaybe<Scalars['Cursor']['input']>;
  condition?: InputMaybe<DemoMessageCondition>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  orderBy?: InputMaybe<Array<DemoMessageOrderBy>>;
};


/** The root query type which gives access points into the data universe. */
export type QueryCharacterArgs = {
  id: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryCharactersArgs = {
  filter?: InputMaybe<FilterCharacter>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryCharactersByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryDemoForumArgs = {
  id: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryDemoForumByRowIdArgs = {
  rowId: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryDemoMessageArgs = {
  id: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryDemoMessageByRowIdArgs = {
  rowId: Scalars['String']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryEpisodeArgs = {
  id: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryEpisodesArgs = {
  filter?: InputMaybe<FilterEpisode>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryEpisodesByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryLocationArgs = {
  id: Scalars['ID']['input'];
};


/** The root query type which gives access points into the data universe. */
export type QueryLocationsArgs = {
  filter?: InputMaybe<FilterLocation>;
  page?: InputMaybe<Scalars['Int']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryLocationsByIdsArgs = {
  ids: Array<Scalars['ID']['input']>;
};


/** The root query type which gives access points into the data universe. */
export type QueryNodeArgs = {
  id: Scalars['ID']['input'];
};

/** The root subscription type: contains realtime events you can subscribe to with the `subscription` operation. */
export type Subscription = {
  __typename?: 'Subscription';
  forumMessage?: Maybe<ForumMessageSubscriptionPayload>;
};


/** The root subscription type: contains realtime events you can subscribe to with the `subscription` operation. */
export type SubscriptionForumMessageArgs = {
  forumId: Scalars['Int']['input'];
};

/** All input for the `updateDemoForumByRowId` mutation. */
export type UpdateDemoForumByRowIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `DemoForum` being updated. */
  demoForumPatch: DemoForumPatch;
  rowId: Scalars['String']['input'];
};

/** All input for the `updateDemoForum` mutation. */
export type UpdateDemoForumInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `DemoForum` being updated. */
  demoForumPatch: DemoForumPatch;
  /** The globally unique `ID` which will identify a single `DemoForum` to be updated. */
  id: Scalars['ID']['input'];
};

/** The output of our update `DemoForum` mutation. */
export type UpdateDemoForumPayload = {
  __typename?: 'UpdateDemoForumPayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `DemoForum` that was updated by this mutation. */
  demoForum?: Maybe<DemoForum>;
  /** An edge for our `DemoForum`. May be used by Relay 1. */
  demoForumEdge?: Maybe<DemoForumEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `DemoForum` mutation. */
export type UpdateDemoForumPayloadDemoForumEdgeArgs = {
  orderBy?: Array<DemoForumOrderBy>;
};

/** All input for the `updateDemoMessageByRowId` mutation. */
export type UpdateDemoMessageByRowIdInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `DemoMessage` being updated. */
  demoMessagePatch: DemoMessagePatch;
  rowId: Scalars['String']['input'];
};

/** All input for the `updateDemoMessage` mutation. */
export type UpdateDemoMessageInput = {
  /**
   * An arbitrary string value with no semantic meaning. Will be included in the
   * payload verbatim. May be used to track mutations by the client.
   */
  clientMutationId?: InputMaybe<Scalars['String']['input']>;
  /** An object where the defined keys will be set on the `DemoMessage` being updated. */
  demoMessagePatch: DemoMessagePatch;
  /** The globally unique `ID` which will identify a single `DemoMessage` to be updated. */
  id: Scalars['ID']['input'];
};

/** The output of our update `DemoMessage` mutation. */
export type UpdateDemoMessagePayload = {
  __typename?: 'UpdateDemoMessagePayload';
  /**
   * The exact same `clientMutationId` that was provided in the mutation input,
   * unchanged and unused. May be used by a client to track mutations.
   */
  clientMutationId?: Maybe<Scalars['String']['output']>;
  /** The `DemoMessage` that was updated by this mutation. */
  demoMessage?: Maybe<DemoMessage>;
  /** An edge for our `DemoMessage`. May be used by Relay 1. */
  demoMessageEdge?: Maybe<DemoMessageEdge>;
  /** Our root query field type. Allows us to run any query from our mutation payload. */
  query?: Maybe<Query>;
};


/** The output of our update `DemoMessage` mutation. */
export type UpdateDemoMessagePayloadDemoMessageEdgeArgs = {
  orderBy?: Array<DemoMessageOrderBy>;
};



export type ResolverTypeWrapper<T> = Promise<T> | T;


export type ResolverWithResolve<TResult, TParent, TContext, TArgs> = {
  resolve: ResolverFn<TResult, TParent, TContext, TArgs>;
};
export type Resolver<TResult, TParent = {}, TContext = {}, TArgs = {}> = ResolverFn<TResult, TParent, TContext, TArgs> | ResolverWithResolve<TResult, TParent, TContext, TArgs>;

export type ResolverFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => Promise<TResult> | TResult;

export type SubscriptionSubscribeFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => AsyncIterable<TResult> | Promise<AsyncIterable<TResult>>;

export type SubscriptionResolveFn<TResult, TParent, TContext, TArgs> = (
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;

export interface SubscriptionSubscriberObject<TResult, TKey extends string, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<{ [key in TKey]: TResult }, TParent, TContext, TArgs>;
  resolve?: SubscriptionResolveFn<TResult, { [key in TKey]: TResult }, TContext, TArgs>;
}

export interface SubscriptionResolverObject<TResult, TParent, TContext, TArgs> {
  subscribe: SubscriptionSubscribeFn<any, TParent, TContext, TArgs>;
  resolve: SubscriptionResolveFn<TResult, any, TContext, TArgs>;
}

export type SubscriptionObject<TResult, TKey extends string, TParent, TContext, TArgs> =
  | SubscriptionSubscriberObject<TResult, TKey, TParent, TContext, TArgs>
  | SubscriptionResolverObject<TResult, TParent, TContext, TArgs>;

export type SubscriptionResolver<TResult, TKey extends string, TParent = {}, TContext = {}, TArgs = {}> =
  | ((...args: any[]) => SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>)
  | SubscriptionObject<TResult, TKey, TParent, TContext, TArgs>;

export type TypeResolveFn<TTypes, TParent = {}, TContext = {}> = (
  parent: TParent,
  context: TContext,
  info: GraphQLResolveInfo
) => Maybe<TTypes> | Promise<Maybe<TTypes>>;

export type IsTypeOfResolverFn<T = {}, TContext = {}> = (obj: T, context: TContext, info: GraphQLResolveInfo) => boolean | Promise<boolean>;

export type NextResolverFn<T> = () => Promise<T>;

export type DirectiveResolverFn<TResult = {}, TParent = {}, TContext = {}, TArgs = {}> = (
  next: NextResolverFn<TResult>,
  parent: TParent,
  args: TArgs,
  context: TContext,
  info: GraphQLResolveInfo
) => TResult | Promise<TResult>;


/** Mapping of interface types */
export type ResolversInterfaceTypes<_RefType extends Record<string, unknown>> = {
  Node: ( DemoForum ) | ( DemoMessage ) | ( Omit<Query, 'characters' | 'node' | 'query'> & { characters?: Maybe<_RefType['Characters']>, node?: Maybe<_RefType['Node']>, query: _RefType['Query'] } );
};

/** Mapping between all available schema types and the resolvers types */
export type ResolversTypes = {
  Boolean: ResolverTypeWrapper<Scalars['Boolean']['output']>;
  CacheControlScope: CacheControlScope;
  Character: ResolverTypeWrapper<Character>;
  Characters: ResolverTypeWrapper<Characters>;
  CreateDemoForumInput: CreateDemoForumInput;
  CreateDemoForumPayload: ResolverTypeWrapper<Omit<CreateDemoForumPayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  CreateDemoMessageInput: CreateDemoMessageInput;
  CreateDemoMessagePayload: ResolverTypeWrapper<Omit<CreateDemoMessagePayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  Cursor: ResolverTypeWrapper<Scalars['Cursor']['output']>;
  DeleteDemoForumByRowIdInput: DeleteDemoForumByRowIdInput;
  DeleteDemoForumInput: DeleteDemoForumInput;
  DeleteDemoForumPayload: ResolverTypeWrapper<Omit<DeleteDemoForumPayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  DeleteDemoMessageByRowIdInput: DeleteDemoMessageByRowIdInput;
  DeleteDemoMessageInput: DeleteDemoMessageInput;
  DeleteDemoMessagePayload: ResolverTypeWrapper<Omit<DeleteDemoMessagePayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  DemoForum: ResolverTypeWrapper<DemoForum>;
  DemoForumCondition: DemoForumCondition;
  DemoForumConnection: ResolverTypeWrapper<DemoForumConnection>;
  DemoForumEdge: ResolverTypeWrapper<DemoForumEdge>;
  DemoForumInput: DemoForumInput;
  DemoForumOrderBy: DemoForumOrderBy;
  DemoForumPatch: DemoForumPatch;
  DemoMessage: ResolverTypeWrapper<DemoMessage>;
  DemoMessageCondition: DemoMessageCondition;
  DemoMessageConnection: ResolverTypeWrapper<DemoMessageConnection>;
  DemoMessageEdge: ResolverTypeWrapper<DemoMessageEdge>;
  DemoMessageInput: DemoMessageInput;
  DemoMessageOrderBy: DemoMessageOrderBy;
  DemoMessagePatch: DemoMessagePatch;
  Episode: ResolverTypeWrapper<Episode>;
  Episodes: ResolverTypeWrapper<Episodes>;
  FilterCharacter: FilterCharacter;
  FilterEpisode: FilterEpisode;
  FilterLocation: FilterLocation;
  ForumMessageSubscriptionPayload: ResolverTypeWrapper<ForumMessageSubscriptionPayload>;
  ID: ResolverTypeWrapper<Scalars['ID']['output']>;
  Info: ResolverTypeWrapper<Info>;
  Int: ResolverTypeWrapper<Scalars['Int']['output']>;
  Location: ResolverTypeWrapper<Location>;
  Locations: ResolverTypeWrapper<Locations>;
  Mutation: ResolverTypeWrapper<{}>;
  Node: ResolverTypeWrapper<ResolversInterfaceTypes<ResolversTypes>['Node']>;
  PageInfo: ResolverTypeWrapper<PageInfo>;
  Query: ResolverTypeWrapper<{}>;
  String: ResolverTypeWrapper<Scalars['String']['output']>;
  Subscription: ResolverTypeWrapper<{}>;
  UpdateDemoForumByRowIdInput: UpdateDemoForumByRowIdInput;
  UpdateDemoForumInput: UpdateDemoForumInput;
  UpdateDemoForumPayload: ResolverTypeWrapper<Omit<UpdateDemoForumPayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  UpdateDemoMessageByRowIdInput: UpdateDemoMessageByRowIdInput;
  UpdateDemoMessageInput: UpdateDemoMessageInput;
  UpdateDemoMessagePayload: ResolverTypeWrapper<Omit<UpdateDemoMessagePayload, 'query'> & { query?: Maybe<ResolversTypes['Query']> }>;
  Upload: ResolverTypeWrapper<Scalars['Upload']['output']>;
};

/** Mapping between all available schema types and the resolvers parents */
export type ResolversParentTypes = {
  Boolean: Scalars['Boolean']['output'];
  Character: Character;
  Characters: Characters;
  CreateDemoForumInput: CreateDemoForumInput;
  CreateDemoForumPayload: Omit<CreateDemoForumPayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  CreateDemoMessageInput: CreateDemoMessageInput;
  CreateDemoMessagePayload: Omit<CreateDemoMessagePayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  Cursor: Scalars['Cursor']['output'];
  DeleteDemoForumByRowIdInput: DeleteDemoForumByRowIdInput;
  DeleteDemoForumInput: DeleteDemoForumInput;
  DeleteDemoForumPayload: Omit<DeleteDemoForumPayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  DeleteDemoMessageByRowIdInput: DeleteDemoMessageByRowIdInput;
  DeleteDemoMessageInput: DeleteDemoMessageInput;
  DeleteDemoMessagePayload: Omit<DeleteDemoMessagePayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  DemoForum: DemoForum;
  DemoForumCondition: DemoForumCondition;
  DemoForumConnection: DemoForumConnection;
  DemoForumEdge: DemoForumEdge;
  DemoForumInput: DemoForumInput;
  DemoForumPatch: DemoForumPatch;
  DemoMessage: DemoMessage;
  DemoMessageCondition: DemoMessageCondition;
  DemoMessageConnection: DemoMessageConnection;
  DemoMessageEdge: DemoMessageEdge;
  DemoMessageInput: DemoMessageInput;
  DemoMessagePatch: DemoMessagePatch;
  Episode: Episode;
  Episodes: Episodes;
  FilterCharacter: FilterCharacter;
  FilterEpisode: FilterEpisode;
  FilterLocation: FilterLocation;
  ForumMessageSubscriptionPayload: ForumMessageSubscriptionPayload;
  ID: Scalars['ID']['output'];
  Info: Info;
  Int: Scalars['Int']['output'];
  Location: Location;
  Locations: Locations;
  Mutation: {};
  Node: ResolversInterfaceTypes<ResolversParentTypes>['Node'];
  PageInfo: PageInfo;
  Query: {};
  String: Scalars['String']['output'];
  Subscription: {};
  UpdateDemoForumByRowIdInput: UpdateDemoForumByRowIdInput;
  UpdateDemoForumInput: UpdateDemoForumInput;
  UpdateDemoForumPayload: Omit<UpdateDemoForumPayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  UpdateDemoMessageByRowIdInput: UpdateDemoMessageByRowIdInput;
  UpdateDemoMessageInput: UpdateDemoMessageInput;
  UpdateDemoMessagePayload: Omit<UpdateDemoMessagePayload, 'query'> & { query?: Maybe<ResolversParentTypes['Query']> };
  Upload: Scalars['Upload']['output'];
};

export type CacheControlDirectiveArgs = {
  maxAge?: Maybe<Scalars['Int']['input']>;
  scope?: Maybe<CacheControlScope>;
};

export type CacheControlDirectiveResolver<Result, Parent, ContextType = any, Args = CacheControlDirectiveArgs> = DirectiveResolverFn<Result, Parent, ContextType, Args>;

export type CharacterResolvers<ContextType = any, ParentType extends ResolversParentTypes['Character'] = ResolversParentTypes['Character']> = {
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  episode?: Resolver<Array<Maybe<ResolversTypes['Episode']>>, ParentType, ContextType>;
  gender?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  image?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  origin?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType>;
  species?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  status?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CharactersResolvers<ContextType = any, ParentType extends ResolversParentTypes['Characters'] = ResolversParentTypes['Characters']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CreateDemoForumPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['CreateDemoForumPayload'] = ResolversParentTypes['CreateDemoForumPayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  demoForum?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType>;
  demoForumEdge?: Resolver<Maybe<ResolversTypes['DemoForumEdge']>, ParentType, ContextType, RequireFields<CreateDemoForumPayloadDemoForumEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type CreateDemoMessagePayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['CreateDemoMessagePayload'] = ResolversParentTypes['CreateDemoMessagePayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  demoMessage?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType>;
  demoMessageEdge?: Resolver<Maybe<ResolversTypes['DemoMessageEdge']>, ParentType, ContextType, RequireFields<CreateDemoMessagePayloadDemoMessageEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface CursorScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Cursor'], any> {
  name: 'Cursor';
}

export type DeleteDemoForumPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['DeleteDemoForumPayload'] = ResolversParentTypes['DeleteDemoForumPayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  deletedDemoForumId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  demoForum?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType>;
  demoForumEdge?: Resolver<Maybe<ResolversTypes['DemoForumEdge']>, ParentType, ContextType, RequireFields<DeleteDemoForumPayloadDemoForumEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DeleteDemoMessagePayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['DeleteDemoMessagePayload'] = ResolversParentTypes['DeleteDemoMessagePayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  deletedDemoMessageId?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  demoMessage?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType>;
  demoMessageEdge?: Resolver<Maybe<ResolversTypes['DemoMessageEdge']>, ParentType, ContextType, RequireFields<DeleteDemoMessagePayloadDemoMessageEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoForumResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoForum'] = ResolversParentTypes['DemoForum']> = {
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  rowId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  title?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoForumConnectionResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoForumConnection'] = ResolversParentTypes['DemoForumConnection']> = {
  edges?: Resolver<Array<Maybe<ResolversTypes['DemoForumEdge']>>, ParentType, ContextType>;
  nodes?: Resolver<Array<Maybe<ResolversTypes['DemoForum']>>, ParentType, ContextType>;
  pageInfo?: Resolver<ResolversTypes['PageInfo'], ParentType, ContextType>;
  totalCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoForumEdgeResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoForumEdge'] = ResolversParentTypes['DemoForumEdge']> = {
  cursor?: Resolver<Maybe<ResolversTypes['Cursor']>, ParentType, ContextType>;
  node?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoMessageResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoMessage'] = ResolversParentTypes['DemoMessage']> = {
  body?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  demoForumByForum?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType>;
  forum?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  rowId?: Resolver<ResolversTypes['String'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoMessageConnectionResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoMessageConnection'] = ResolversParentTypes['DemoMessageConnection']> = {
  edges?: Resolver<Array<Maybe<ResolversTypes['DemoMessageEdge']>>, ParentType, ContextType>;
  nodes?: Resolver<Array<Maybe<ResolversTypes['DemoMessage']>>, ParentType, ContextType>;
  pageInfo?: Resolver<ResolversTypes['PageInfo'], ParentType, ContextType>;
  totalCount?: Resolver<ResolversTypes['Int'], ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type DemoMessageEdgeResolvers<ContextType = any, ParentType extends ResolversParentTypes['DemoMessageEdge'] = ResolversParentTypes['DemoMessageEdge']> = {
  cursor?: Resolver<Maybe<ResolversTypes['Cursor']>, ParentType, ContextType>;
  node?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EpisodeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Episode'] = ResolversParentTypes['Episode']> = {
  air_date?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  characters?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  episode?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type EpisodesResolvers<ContextType = any, ParentType extends ResolversParentTypes['Episodes'] = ResolversParentTypes['Episodes']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type ForumMessageSubscriptionPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['ForumMessageSubscriptionPayload'] = ResolversParentTypes['ForumMessageSubscriptionPayload']> = {
  event?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  message?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type InfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['Info'] = ResolversParentTypes['Info']> = {
  count?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  next?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  pages?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  prev?: Resolver<Maybe<ResolversTypes['Int']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LocationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Location'] = ResolversParentTypes['Location']> = {
  created?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  dimension?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  id?: Resolver<Maybe<ResolversTypes['ID']>, ParentType, ContextType>;
  name?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  residents?: Resolver<Array<Maybe<ResolversTypes['Character']>>, ParentType, ContextType>;
  type?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type LocationsResolvers<ContextType = any, ParentType extends ResolversParentTypes['Locations'] = ResolversParentTypes['Locations']> = {
  info?: Resolver<Maybe<ResolversTypes['Info']>, ParentType, ContextType>;
  results?: Resolver<Maybe<Array<Maybe<ResolversTypes['Location']>>>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type MutationResolvers<ContextType = any, ParentType extends ResolversParentTypes['Mutation'] = ResolversParentTypes['Mutation']> = {
  createDemoForum?: Resolver<Maybe<ResolversTypes['CreateDemoForumPayload']>, ParentType, ContextType, RequireFields<MutationCreateDemoForumArgs, 'input'>>;
  createDemoMessage?: Resolver<Maybe<ResolversTypes['CreateDemoMessagePayload']>, ParentType, ContextType, RequireFields<MutationCreateDemoMessageArgs, 'input'>>;
  deleteDemoForum?: Resolver<Maybe<ResolversTypes['DeleteDemoForumPayload']>, ParentType, ContextType, RequireFields<MutationDeleteDemoForumArgs, 'input'>>;
  deleteDemoForumByRowId?: Resolver<Maybe<ResolversTypes['DeleteDemoForumPayload']>, ParentType, ContextType, RequireFields<MutationDeleteDemoForumByRowIdArgs, 'input'>>;
  deleteDemoMessage?: Resolver<Maybe<ResolversTypes['DeleteDemoMessagePayload']>, ParentType, ContextType, RequireFields<MutationDeleteDemoMessageArgs, 'input'>>;
  deleteDemoMessageByRowId?: Resolver<Maybe<ResolversTypes['DeleteDemoMessagePayload']>, ParentType, ContextType, RequireFields<MutationDeleteDemoMessageByRowIdArgs, 'input'>>;
  updateDemoForum?: Resolver<Maybe<ResolversTypes['UpdateDemoForumPayload']>, ParentType, ContextType, RequireFields<MutationUpdateDemoForumArgs, 'input'>>;
  updateDemoForumByRowId?: Resolver<Maybe<ResolversTypes['UpdateDemoForumPayload']>, ParentType, ContextType, RequireFields<MutationUpdateDemoForumByRowIdArgs, 'input'>>;
  updateDemoMessage?: Resolver<Maybe<ResolversTypes['UpdateDemoMessagePayload']>, ParentType, ContextType, RequireFields<MutationUpdateDemoMessageArgs, 'input'>>;
  updateDemoMessageByRowId?: Resolver<Maybe<ResolversTypes['UpdateDemoMessagePayload']>, ParentType, ContextType, RequireFields<MutationUpdateDemoMessageByRowIdArgs, 'input'>>;
};

export type NodeResolvers<ContextType = any, ParentType extends ResolversParentTypes['Node'] = ResolversParentTypes['Node']> = {
  __resolveType: TypeResolveFn<'DemoForum' | 'DemoMessage' | 'Query', ParentType, ContextType>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
};

export type PageInfoResolvers<ContextType = any, ParentType extends ResolversParentTypes['PageInfo'] = ResolversParentTypes['PageInfo']> = {
  endCursor?: Resolver<Maybe<ResolversTypes['Cursor']>, ParentType, ContextType>;
  hasNextPage?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  hasPreviousPage?: Resolver<ResolversTypes['Boolean'], ParentType, ContextType>;
  startCursor?: Resolver<Maybe<ResolversTypes['Cursor']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type QueryResolvers<ContextType = any, ParentType extends ResolversParentTypes['Query'] = ResolversParentTypes['Query']> = {
  allDemoForums?: Resolver<Maybe<ResolversTypes['DemoForumConnection']>, ParentType, ContextType, RequireFields<QueryAllDemoForumsArgs, 'orderBy'>>;
  allDemoMessages?: Resolver<Maybe<ResolversTypes['DemoMessageConnection']>, ParentType, ContextType, RequireFields<QueryAllDemoMessagesArgs, 'orderBy'>>;
  character?: Resolver<Maybe<ResolversTypes['Character']>, ParentType, ContextType, RequireFields<QueryCharacterArgs, 'id'>>;
  characters?: Resolver<Maybe<ResolversTypes['Characters']>, ParentType, ContextType, Partial<QueryCharactersArgs>>;
  charactersByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Character']>>>, ParentType, ContextType, RequireFields<QueryCharactersByIdsArgs, 'ids'>>;
  demoForum?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType, RequireFields<QueryDemoForumArgs, 'id'>>;
  demoForumByRowId?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType, RequireFields<QueryDemoForumByRowIdArgs, 'rowId'>>;
  demoMessage?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType, RequireFields<QueryDemoMessageArgs, 'id'>>;
  demoMessageByRowId?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType, RequireFields<QueryDemoMessageByRowIdArgs, 'rowId'>>;
  episode?: Resolver<Maybe<ResolversTypes['Episode']>, ParentType, ContextType, RequireFields<QueryEpisodeArgs, 'id'>>;
  episodes?: Resolver<Maybe<ResolversTypes['Episodes']>, ParentType, ContextType, Partial<QueryEpisodesArgs>>;
  episodesByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Episode']>>>, ParentType, ContextType, RequireFields<QueryEpisodesByIdsArgs, 'ids'>>;
  id?: Resolver<ResolversTypes['ID'], ParentType, ContextType>;
  location?: Resolver<Maybe<ResolversTypes['Location']>, ParentType, ContextType, RequireFields<QueryLocationArgs, 'id'>>;
  locations?: Resolver<Maybe<ResolversTypes['Locations']>, ParentType, ContextType, Partial<QueryLocationsArgs>>;
  locationsByIds?: Resolver<Maybe<Array<Maybe<ResolversTypes['Location']>>>, ParentType, ContextType, RequireFields<QueryLocationsByIdsArgs, 'ids'>>;
  node?: Resolver<Maybe<ResolversTypes['Node']>, ParentType, ContextType, RequireFields<QueryNodeArgs, 'id'>>;
  query?: Resolver<ResolversTypes['Query'], ParentType, ContextType>;
};

export type SubscriptionResolvers<ContextType = any, ParentType extends ResolversParentTypes['Subscription'] = ResolversParentTypes['Subscription']> = {
  forumMessage?: SubscriptionResolver<Maybe<ResolversTypes['ForumMessageSubscriptionPayload']>, "forumMessage", ParentType, ContextType, RequireFields<SubscriptionForumMessageArgs, 'forumId'>>;
};

export type UpdateDemoForumPayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['UpdateDemoForumPayload'] = ResolversParentTypes['UpdateDemoForumPayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  demoForum?: Resolver<Maybe<ResolversTypes['DemoForum']>, ParentType, ContextType>;
  demoForumEdge?: Resolver<Maybe<ResolversTypes['DemoForumEdge']>, ParentType, ContextType, RequireFields<UpdateDemoForumPayloadDemoForumEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export type UpdateDemoMessagePayloadResolvers<ContextType = any, ParentType extends ResolversParentTypes['UpdateDemoMessagePayload'] = ResolversParentTypes['UpdateDemoMessagePayload']> = {
  clientMutationId?: Resolver<Maybe<ResolversTypes['String']>, ParentType, ContextType>;
  demoMessage?: Resolver<Maybe<ResolversTypes['DemoMessage']>, ParentType, ContextType>;
  demoMessageEdge?: Resolver<Maybe<ResolversTypes['DemoMessageEdge']>, ParentType, ContextType, RequireFields<UpdateDemoMessagePayloadDemoMessageEdgeArgs, 'orderBy'>>;
  query?: Resolver<Maybe<ResolversTypes['Query']>, ParentType, ContextType>;
  __isTypeOf?: IsTypeOfResolverFn<ParentType, ContextType>;
};

export interface UploadScalarConfig extends GraphQLScalarTypeConfig<ResolversTypes['Upload'], any> {
  name: 'Upload';
}

export type Resolvers<ContextType = any> = {
  Character?: CharacterResolvers<ContextType>;
  Characters?: CharactersResolvers<ContextType>;
  CreateDemoForumPayload?: CreateDemoForumPayloadResolvers<ContextType>;
  CreateDemoMessagePayload?: CreateDemoMessagePayloadResolvers<ContextType>;
  Cursor?: GraphQLScalarType;
  DeleteDemoForumPayload?: DeleteDemoForumPayloadResolvers<ContextType>;
  DeleteDemoMessagePayload?: DeleteDemoMessagePayloadResolvers<ContextType>;
  DemoForum?: DemoForumResolvers<ContextType>;
  DemoForumConnection?: DemoForumConnectionResolvers<ContextType>;
  DemoForumEdge?: DemoForumEdgeResolvers<ContextType>;
  DemoMessage?: DemoMessageResolvers<ContextType>;
  DemoMessageConnection?: DemoMessageConnectionResolvers<ContextType>;
  DemoMessageEdge?: DemoMessageEdgeResolvers<ContextType>;
  Episode?: EpisodeResolvers<ContextType>;
  Episodes?: EpisodesResolvers<ContextType>;
  ForumMessageSubscriptionPayload?: ForumMessageSubscriptionPayloadResolvers<ContextType>;
  Info?: InfoResolvers<ContextType>;
  Location?: LocationResolvers<ContextType>;
  Locations?: LocationsResolvers<ContextType>;
  Mutation?: MutationResolvers<ContextType>;
  Node?: NodeResolvers<ContextType>;
  PageInfo?: PageInfoResolvers<ContextType>;
  Query?: QueryResolvers<ContextType>;
  Subscription?: SubscriptionResolvers<ContextType>;
  UpdateDemoForumPayload?: UpdateDemoForumPayloadResolvers<ContextType>;
  UpdateDemoMessagePayload?: UpdateDemoMessagePayloadResolvers<ContextType>;
  Upload?: GraphQLScalarType;
};

export type DirectiveResolvers<ContextType = any> = {
  cacheControl?: CacheControlDirectiveResolver<any, any, ContextType>;
};

export const namedOperations = {
  Query: {
    __regenerate: '__regenerate',
    Test: 'Test'
  },
  Subscription: {
    MySubscription: 'MySubscription'
  }
}
export type MySubscriptionSubscriptionVariables = Exact<{ [key: string]: never; }>;


export type MySubscriptionSubscription = { __typename?: 'Subscription', forumMessage?: { __typename?: 'ForumMessageSubscriptionPayload', event?: string | null, message?: { __typename?: 'DemoMessage', id: string, body: string } | null } | null };

export type __RegenerateQueryVariables = Exact<{ [key: string]: never; }>;


export type __RegenerateQuery = { __typename: 'Query' };

export type TestQueryVariables = Exact<{ [key: string]: never; }>;


export type TestQuery = { __typename: 'Query' };


export const MySubscriptionDocument = gql`
    subscription MySubscription {
  forumMessage(forumId: 1) {
    message {
      id
      body
    }
    event
  }
}
    `;

/**
 * __useMySubscriptionSubscription__
 *
 * To run a query within a Vue component, call `useMySubscriptionSubscription` and pass it any options that fit your needs.
 * When your component renders, `useMySubscriptionSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useMySubscriptionSubscription();
 */
export function useMySubscriptionSubscription(options: VueApolloComposable.UseSubscriptionOptions<MySubscriptionSubscription, MySubscriptionSubscriptionVariables> | VueCompositionApi.Ref<VueApolloComposable.UseSubscriptionOptions<MySubscriptionSubscription, MySubscriptionSubscriptionVariables>> | ReactiveFunction<VueApolloComposable.UseSubscriptionOptions<MySubscriptionSubscription, MySubscriptionSubscriptionVariables>> = {}) {
  return VueApolloComposable.useSubscription<MySubscriptionSubscription, MySubscriptionSubscriptionVariables>(MySubscriptionDocument, {}, options);
}
export type MySubscriptionSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<MySubscriptionSubscription, MySubscriptionSubscriptionVariables>;
export const __RegenerateDocument = gql`
    query __regenerate {
  __typename
}
    `;

/**
 * __use__RegenerateQuery__
 *
 * To run a query within a Vue component, call `use__RegenerateQuery` and pass it any options that fit your needs.
 * When your component renders, `use__RegenerateQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = use__RegenerateQuery();
 */
export function use__RegenerateQuery(options: VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<__RegenerateQuery, __RegenerateQueryVariables>(__RegenerateDocument, {}, options);
}
export function use__RegenerateLazyQuery(options: VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<__RegenerateQuery, __RegenerateQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<__RegenerateQuery, __RegenerateQueryVariables>(__RegenerateDocument, {}, options);
}
export type __RegenerateQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<__RegenerateQuery, __RegenerateQueryVariables>;
export const TestDocument = gql`
    query Test {
  __typename
}
    `;

/**
 * __useTestQuery__
 *
 * To run a query within a Vue component, call `useTestQuery` and pass it any options that fit your needs.
 * When your component renders, `useTestQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useTestQuery();
 */
export function useTestQuery(options: VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables>> = {}) {
  return VueApolloComposable.useQuery<TestQuery, TestQueryVariables>(TestDocument, {}, options);
}
export function useTestLazyQuery(options: VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables> | VueCompositionApi.Ref<VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables>> | ReactiveFunction<VueApolloComposable.UseQueryOptions<TestQuery, TestQueryVariables>> = {}) {
  return VueApolloComposable.useLazyQuery<TestQuery, TestQueryVariables>(TestDocument, {}, options);
}
export type TestQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<TestQuery, TestQueryVariables>;

export const MySubscription = gql`
    subscription MySubscription {
  forumMessage(forumId: 1) {
    message {
      id
      body
    }
    event
  }
}
    `;
export const __Regenerate = gql`
    query __regenerate {
  __typename
}
    `;
export const Test = gql`
    query Test {
  __typename
}
    `;