// Define unique development server port per each app.
// Don't forget to expose this port in `compose.yml`.
const port = 3000

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },

  devServer: {
    port
  },

  vite: {
    server: {
      hmr: {
        // Addresses a very minor Nuxt bug on Nuxt config reload
        // sometimes falling back to 24678 WS port
        port
      }
    }
  },

  extends: [
    `${process.env.SDK_APP_PATH}/demo`,
  ],

  $production: {
    vuelo: {
      storybook: {
        proxy: true
      }
    }
  }
})
