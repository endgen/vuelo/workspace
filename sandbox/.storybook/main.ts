import type { StorybookConfig } from "@storybook-vue/nuxt"
import { mergeConfig } from 'vite'
import layers from '../.nuxt/vuelo/layers'

const base = '/storybook/'

const config: StorybookConfig = {
  stories: [
    "../stories/**/*.mdx",
    "../stories/**/*.stories.@(js|jsx|mjs|ts|tsx)",
    ...layers.map(({ path }) => [
      `${path}/**/*.mdx`,
      `${path}/**/*.stories.@(js|jsx|ts|tsx)`
    ]).flat()
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/addon-interactions",
  ],
  framework: {
    name: "@storybook-vue/nuxt",
    options: {},
  },
  docs: {
    autodocs: "tag",
  },
  // managerHead: (head, { configType }) => {
  //   return (`
  //     ${head}
  //     <base href="${base}">
  //   `)
  // },
  // async viteFinal(config, { configType }) {
  //   return mergeConfig(
  //     {
  //       base
  //     },
  //     config
  //   )
  // }
};
export default config;
