-- Create security roles
-- https://www.jujens.eu/posts/en/2021/Mar/10/db-user-migrations/
-- https://dba.stackexchange.com/questions/95867/grant-usage-on-all-schemas-in-a-database


CREATE ROLE application;
CREATE ROLE broadcaster;

CREATE ROLE front_end;
CREATE ROLE back_end;

CREATE USER admin CREATEROLE CREATEDB;

-- TODO: Figure out proper connector setup
-- https://debezium.io/documentation/reference/stable/connectors/postgresql.html#postgresql-permissions
-- CREATE USER connector REPLICATION;
CREATE USER connector SUPERUSER;

CREATE USER api;
CREATE USER graphql;
CREATE USER consumer;

GRANT application TO front_end;
GRANT application TO back_end;

GRANT broadcaster TO admin;
GRANT broadcaster TO connector;

GRANT front_end TO graphql;
GRANT front_end TO api;

GRANT back_end TO consumer;

ALTER DEFAULT PRIVILEGES
  GRANT SELECT, INSERT, UPDATE, DELETE
  ON TABLES
  TO admin;

ALTER DEFAULT PRIVILEGES
  GRANT USAGE, SELECT
  ON SEQUENCES
  TO admin;

ALTER DEFAULT PRIVILEGES
  REVOKE GRANT OPTION
  FOR ALL PRIVILEGES
  ON TABLES
  FROM admin;